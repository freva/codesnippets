# CodeSnippets

A collection of Algorithms/Architectures in Jupyter Notebooks.

This repository should serve as a random collection of examples for algorithms and other building blocks for any kind of data processing. The code is presented in notebook format giving examples and explanation.

You can clone this repository:

```bash
git clone https://gitlab.dkrz.de/freva/codesnippets.git
```
## Installation:

To be able to run all notebooks install the kernel specs in your home on mistral:

```bash
/work/ch1187/regiklim-ces/freva/xarray/bin/python -m ipykernel install --user --name python3_codesnippets --display-name=python3_codesnippets
```

## List of Content:

* [Masking Regions in gridded data sets using shapefiles](https://gitlab.dkrz.de/freva/codesnippets/-/blob/master/notebooks/MaskRegionFromShapeFile.ipynb)
* [Reduce the time dimension (mean or sum) of data with two types of different missing values](https://gitlab.dkrz.de/freva/plugins4freva/codesnippets/-/blob/master/notebooks/AvgOrSumDataWithMissingValues.ipynb)
